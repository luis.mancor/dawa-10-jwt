const jwt = require("jsonwebtoken");

//Verifica token

let verificaToken = (req,res,next) => {

    let token = req.get("token");

    jwt.verify(token, process.env.SEED, (err, decoded) => {
        
        console.log(process.env.SEED);
        if (err){
            return res.status(400).json({
                ok:false,
                err,
            });
        }

        req.usuario  = decoded.usuario;
        console.log(req.usuario);
        console.log("--------------");
        console.log(decoded.usuario);

        console.log(decoded);
        next();
    });

};

module.exports = {verificaToken,};
